#include "read.h"

#include <assert.h>
#include <string.h>


/* Regex */

static const char* WHITESPACE_REGEX = "[\\s]+|;.*";
static const char* DELIMITER_REGEX = "[()]";
static const char* SYMBOL_REGEX = ("[^\\s()\\[\\]{}\",'`;#\\|\\\\.]"
                                   "[^\\s()\\[\\]{}\",'`;#\\|\\\\]*");
static const char* BOOLEAN_REGEX = "#[tf]";
static const char* INTEGER_REGEX = "[-+]?\\d+";
static const char* STRING_REGEX = "\"[^\"]*\"";
static const char* READER_MACRO_REGEX = "'";


/* Atom readers */

typedef struct {
    bool success;
    LispValue result;
} ReadTuple;

static ReadTuple read_atom_bool(String t) {
    ReadTuple ret;
    if (gcstr_eq(t, "#t")) {
        ret.success = true;
        ret.result = c_make_boolean(true);
    } else {
        ret.success = true;
        ret.result = c_make_boolean(false);
    }
    return ret;
}

static ReadTuple read_atom_int(String t) {
    ReadTuple ret = {true, c_make_integer(gcstr_tol(t))};
    return ret;
}

static ReadTuple read_atom_string(String t) {
    size_t len = gcstr_len(t);
    assert(len >= 2);
    ReadTuple ret = {true, c_make_string(gcstr_slice(t, 1, len - 1))};
    return ret;
}

static ReadTuple read_atom_symbol(String t) {
    ReadTuple ret = {true, c_make_symbol(t)};
    return ret;
}


/* Main reader */

typedef struct {
    const char* data;
    String token;
} Reader;

static void reader_bind(Reader* r, const char* token) {
    r->token = gcstr_new(token);
}

static bool reader_eof(Reader* r) {
    return *(r->data) == '\0';
}

static bool reader_match_regex(Reader* r, const char* regex) {
    if (reader_eof(r)) {
        return false;
    }

    RegexResult m = regex_match(r->data, regex);
    if ((m.match_count < 0) || (m.ovector[0] != 0)) {
        /* No matches found, or match is not at start of string */
        regex_cleanup(m);
        return false;
    }

    char* mstr = regex_result_get_match(m, 0);
    reader_bind(r, mstr);

    free(mstr);
    regex_cleanup(m);

    return true;
}

static void reader_skip(Reader* r) {
    if (r->token != NULL) {
        r->data += gcstr_len(r->token);
    }
}

static void reader_next(Reader* r) {
    reader_skip(r);

    /* Skip over whitespace */
    while (reader_match_regex(r, WHITESPACE_REGEX)) {
        reader_skip(r);
    }

    /* Check that the input hasn't ended yet */
    if (reader_eof(r)) {
        return;
    }

    /* Match macros */
    if (reader_match_regex(r, READER_MACRO_REGEX)) {
        return;
    }

    /* Match delimited expressions */
    if (reader_match_regex(r, DELIMITER_REGEX)) {
        return;
    }

    /* Match atomic data types */
    if (reader_match_regex(r, SYMBOL_REGEX)
        || reader_match_regex(r, BOOLEAN_REGEX)
        || reader_match_regex(r, INTEGER_REGEX)
        || reader_match_regex(r, STRING_REGEX)) {
        return;
    }

    /* If we have arrived here, there is string left that doesn't */
    /* match any token: a syntax error of some kind. Bind token */
    /* nonetheless and handle the problem in read_atom. */
    reader_bind(r, r->data);
}


/* Lisp reader functions (that use the main reader) */

static ReadTuple read_form(Reader*);

static ReadTuple process_reader_macro(Reader* r, const char* symbol) {
    if (reader_eof(r)) {
        ReadTuple err = {
            false, c_lsprintf(("syntax error: expected argument "
                               "for reader macro, got EOF"))
        };
        return err;
    }

    ReadTuple macro_arg = read_form(r);
    if (!macro_arg.success) {
        return macro_arg;
    }

    LispValue sym = c_make_symbol(gcstr_new(symbol));
    ReadTuple ret = {
        true, c_make_pair(sym, c_make_pair(macro_arg.result, c_make_empty()))
    };
    return ret;
}

static ReadTuple read_atom(Reader* r) {
    String token = r->token;
    reader_next(r);

    if (gcstr_eq(token, "'")) {
        return process_reader_macro(r, "quote");
    }

    if (gcstr_regex_match(token, INTEGER_REGEX, true)) {
        return read_atom_int(token);
    } else if (gcstr_regex_match(token, BOOLEAN_REGEX, true)) {
        return read_atom_bool(token);
    } else if (gcstr_regex_match(token, STRING_REGEX, true)) {
        return read_atom_string(token);
    } else if (gcstr_regex_match(token, SYMBOL_REGEX, true)) {
        return read_atom_symbol(token);
    }

    ReadTuple err = {
        false, c_lsprintf("syntax error: %s", gcstr_data(token))
    };
    return err;
}

static ReadTuple read_list(Reader* r, const char* close) {
    reader_next(r);

    LispValue result = c_make_empty();
    ReadTuple form;
    while (true) {
        if (reader_eof(r)) {
            ReadTuple err = {
                false,
                c_lsprintf("syntax error: expected '%s', got EOF", close)
            };
            return err;
        }

        if (gcstr_eq(r->token, close)) {
            reader_next(r);
            ReadTuple ret = {true, c_list_reverse(result)};
            return ret;
        }

        form = read_form(r);
        if (!form.success) {
            return form;
        }
        result = c_make_pair(form.result, result);
    }
}

static ReadTuple read_form(Reader* r) {
    if (gcstr_eq(r->token, "(")) {
        return read_list(r, ")");
    }

    /* Check for stray closing delimiters */
    if (gcstr_eq(r->token, ")")) {
        ReadTuple err = {
            false, c_lsprintf("syntax error: unexpected ')'")
        };
        return err;
    }

    return read_atom(r);
}


/* Interface functions */

bool l_read_from_string(LispValue* ret, const char* input) {
    /* Initialize the Reader */
    Reader r = {input, NULL};
    reader_next(&r);

    /* Read the input forms */
    LispValue result = c_make_empty();
    ReadTuple form;
    while (true) {
        if (reader_eof(&r)) {
            *ret = c_list_reverse(result);
            return true;
        }

        form = read_form(&r);
        if (!form.success) {
            *ret = form.result;
            return false;
        }
        result = c_make_pair(form.result, result);
    }
}
