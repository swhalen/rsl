#include <argp.h>

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <stdbool.h>

#include <readline/readline.h>
#include <readline/history.h>
#include <gc/gc.h>

#include "values.h"
#include "read.h"
#include "eval.h"
#include "lib.h"
#include "cps_transform.h"

#include "../gitversion.c"

/* Argp setup */

static char doc[] = "Really small/slow lisp.";

static char args_doc[] = "filename";

static struct argp_option options[] = {
    {"cps", 'c', 0, 0, "Program is written in continuation-passing style.", 0},
    {0}
};

struct arguments {
    char* filename;
    bool cps;
};

static error_t parse_opt(int key, char* arg, struct argp_state* state) {
    struct arguments *arguments = state->input;
    switch (key) {
    case 'c':
        arguments->cps = true;
        break;
    case ARGP_KEY_ARG:
        if (state->arg_num > 0) {
            /* Too many arguments */
            argp_usage(state);
        }
        arguments->filename = arg;
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};

/* Helpers */

static void repl_err_toplevel(LispValue message) {
    printf("; unhandled error: ");
    c_print(message, false, false);
    printf("\n");
}

static void handle_input(bool success, LispValue result, Environment env,
                         bool cps_style) {
    /* Check for an error in reading */
    if (!success) {
        repl_err_toplevel(result);
        return;
    }

    /* Empty input is just ignored */
    if (c_empty_p(result)) {
        return;
    }

    /* Wrap the input in a (begin ...) so that multiple forms can be
     * handled. */
    result = c_make_pair(c_make_symbol(gcstr_new("begin")), result);

    if (!cps_style) {
        /* Transform the program to continuation-passing style. */
        result = c_cps_transform(result);
    }

    /* This is a procedure, so we apply it. If the
     * application is successful, there won't be a valid
     * return value, so we only invoke the REPL error
     * callback in case the success flag comes back
     * `false`. */
    if (!l_eval_application(&result, result, env)) {
        repl_err_toplevel(result);
        return;
    }
}

/* Main */

int main(int argc, char** argv) {
    /* Parse command-line args */
    struct arguments arguments;
    arguments.filename = NULL;
    arguments.cps = false;
    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    /* Start the interpreter */
    GC_INIT();

    Environment env = make_default_environment();

    if (arguments.filename == NULL) {
        /* stdin is a terminal, so start the REPL */
        char* input;
        bool success;
        LispValue result = NULL;

        if (isatty(STDIN_FILENO)) {
            using_history();

            while (true) {
                input = readline("> ");
                if (input == NULL) {
                    /* Quit if NULL input (i.e., C-d) */
                    printf("\n");
                    break;
                }
                success = l_read_from_string(&result, input);
                add_history(input);
                free(input);
                handle_input(success, result, env, arguments.cps);
            }
        } else {
            char* line;
            size_t bufsize = 0;
            ssize_t read;

            size_t read_total = 1;
            char* input = malloc(read_total);
            input[0] = '\0';

            while (true) {
                line = NULL;
                read = getline(&line, &bufsize, stdin);

                if (read == -1) {
                    /* End of input */
                    break;
                }

                read_total += read;
                input = realloc(input, read_total);
                strncat(input, line, read);
                free(line);
            }

            success = l_read_from_string(&result, input);
            add_history(input);
            free(input);
            handle_input(success, result, env, arguments.cps);
        }
    } else if (arguments.filename != NULL) {
        /* Read the specified file */
        FILE* fp = fopen(arguments.filename, "r");
        if (fp == NULL || ferror(fp)) {
            printf("Error during file reading.\n");
            exit(EXIT_FAILURE);
        }
        char* buffer = NULL;
        size_t len;
        ssize_t bytes_read = getdelim(&buffer, &len, '\0', fp);
        fclose(fp);

        if (bytes_read != -1) {
            /* The file has been read successfully, so parse it and
             * execute the result */
            LispValue result = NULL;
            bool success = l_read_from_string(&result, buffer);
            handle_input(success, result, env, arguments.cps);
        }

        free(buffer);
    }

    exit(EXIT_SUCCESS);
}
