#include <assert.h>

#include "eval.h"

#include <stdio.h>
#include <gc/gc.h>


/* Stack data structure */

typedef struct _Stack *Stack;

struct _Stack {
    LispValue form;
    Environment env;

    Stack next;
};


static Stack stack_push(Stack stack, LispValue val, Environment env) {
    Stack result = GC_MALLOC(sizeof(*result));

    result->form = val;
    result->env = env;
    result->next = stack;

    return result;
}


/* Evaluation producedures */

static bool c_self_evaluating_p(LispValue exp) {
    return (c_void_p(exp) || c_string_p(exp) || c_boolean_p(exp) ||
            c_integer_p(exp));
}


static bool l_eval_special_quote(LispValue* ret, LispValue exp) {
    long num_args = c_list_length(exp);
    if (num_args != 1) {
        /* The reader should prevent this from ever happening */
        *ret = c_lsprintf("quote: wrong number of arguments");
        return false;
    }

    *ret = c_first(exp);
    return true;
}

static bool l_eval_special_lambda(LispValue* ret, LispValue exp,
                                  Environment env) {
    long num_args = c_list_length(exp);
    if (num_args != 2) {
        *ret = c_lsprintf("lambda: wrong number of arguments");
        return false;
    }

    LispValue params = c_first(exp);
    LispValue body = c_first(c_rest(exp));

    if (!c_list_p(params)) {
        *ret = c_lsprintf("lambda: parameters must be a list");
        return false;
    }
    if (!c_pair_p(body)) {
        *ret = c_lsprintf("lambda: body must be a form");
        return false;
    }

    LispValue params_test = params;
    while (!c_empty_p(params_test)) {
        if (!c_symbol_p(c_first(params_test))) {
            *ret = c_lsprintf("lambda: parameters must be symbols");
            return false;
        }
        params_test = c_rest(params_test);
    }

    *ret = c_make_lambda(params, body, env);
    return true;
}


static bool l_eval_compound_value(LispValue* ret, LispValue exp,
                                  Environment env) {
    /* If a compound form appears in a context where a value is
     * required, this function will be called. If this turns out to be
     * a special form, we evaluate; otherwise an error is raised. */

    assert(c_pair_p(exp) && c_list_length(exp) >= 1);

    LispValue head, tail;
    head = c_first(exp);
    tail = c_rest(exp);

    if (c_symbol_p(head)) {
        if (gcstr_eq(c_symbol_value(head), "quote")) {
            return l_eval_special_quote(ret, tail);
        }

        if (gcstr_eq(c_symbol_value(head), "lambda")) {
            return l_eval_special_lambda(ret, tail, env);
        }
    }
    *ret = c_lsprintf("eval: not a value");
    return false;
}


static bool c_reserved_name_p(LispValue sym) {
    assert(c_symbol_p(sym));
    return (gcstr_eq(c_symbol_value(sym), "quote")
            || gcstr_eq(c_symbol_value(sym), "lambda")
            || gcstr_eq(c_symbol_value(sym), "begin")
            || gcstr_eq(c_symbol_value(sym), "define")
            || gcstr_eq(c_symbol_value(sym), "eval"));
}


static bool l_eval_value(LispValue* ret, LispValue exp, Environment env) {
    if (c_self_evaluating_p(exp)) {
        *ret = exp;
        return true;
    }

    if (c_symbol_p(exp)) {
        if (c_reserved_name_p(exp)) {
            *ret = c_lsprintf("cannot reference reserved name '%s'",
                              gcstr_data(c_symbol_value(exp)));
            return false;
        }

        return l_environment_lookup(ret, env, exp);
    }

    if (c_pair_p(exp)) {
        return l_eval_compound_value(ret, exp, env);
    }

    *ret = c_lsprintf("not a value");
    return false;
}


static bool l_eval_arglist(LispValue* ret, LispValue args,
                           Environment env) {
    assert(c_list_p(args));
    LispValue result = c_make_empty();

    if (c_empty_p(args)) {
        *ret = result;
        return true;
    }

    LispValue car;
    LispValue cdr;
    LispValue evaluated;
    while (true) {
        car = c_first(args);
        cdr = c_rest(args);

        if (!l_eval_value(&evaluated, car, env)) {
            *ret = evaluated;
            return false;
        }
        result = c_make_pair(evaluated, result);

        if (c_empty_p(cdr)) {
            *ret = c_list_reverse(result);
            return true;
        } else {
            args = cdr;
        }
    }
}


static bool l_apply_boolean(
    LispValue* ret,
    LispValue* next_proc, LispValue* next_args,
    LispValue proc, LispValue args, Environment env)
{
    /* Check that we have a well-formed boolean application */
    if (c_list_length(args) != 2) {
        *ret = c_lsprintf("boolean: wrong number of arguments");
        return false;
    }

    /* Evaluate args */
    if (!l_eval_arglist(&args, args, env)) {
        *ret = args;
        return false;
    }

    /* Destructure and check that both consequent and alternative are
       lambdas */
    LispValue consequent = c_first(args);
    LispValue alternative = c_first(c_rest(args));
    if (!(c_lambda_p(consequent) & c_lambda_p(alternative))) {
        *ret = c_lsprintf("boolean: consequent and alternative must be forms");
        return false;
    }

    /* Choose the next form based on whether we have #t or #f in head
       position. Boolean continuations don't take any args (they are
       "thunks") so *next_args becomes '() */
    if (c_boolean_value(proc)) {
        *next_proc = consequent;
    } else {
        *next_proc = alternative;
    }
    *next_args = c_make_empty();
    return true;
}


static bool l_apply_primitive(
    LispValue* ret,
    LispValue* next_proc, LispValue* next_args,
    LispValue proc, LispValue args, Environment env)
{
    assert(c_primitive_p(proc));
    assert(c_list_p(args));

    /* Check that the continuation is there */
    if ((!c_primitive_direct(proc)) && (c_list_length(args) < 1)) {
        *ret = c_lsprintf("primitive procedure requires continuation");
        return false;
    }

    /* Evaluate args */
    if (!l_eval_arglist(&args, args, env)) {
        *ret = args;
        return false;
    }

    /* Destructure as necessary */
    LispValue true_args;
    if (c_primitive_direct(proc)) {
        true_args = args;
    } else {
        true_args = c_rest(args);
    }

    if ((c_primitive_func(proc))(ret, true_args)) {
        /* The underlying primitive succeeded, so we can set the
           continuation if appropriate, set *ret to NULL and return
           true */
        if (c_primitive_direct(proc)) {
            /* This is a direct primitive, so don't actually set the
             * continuation; in fact set it to NULL so that evaluation
             * terminates */
            *next_proc = NULL;
            *next_args = NULL;
        } else {
            /* This is a "non-direct" primitive, so we set the
               continuation. Primitives return a single value, so we
               make a pair here. */
            *next_proc = c_first(args);
            *next_args = c_make_pair(*ret, c_make_empty());
        }

        *ret = NULL;
        return true;
    } else {
        /* The underlying primitive failed */
        return false;
    }
}


static bool l_eval_procedure(LispValue* ret, LispValue proc,
                             Environment env) {
    /* Special version of l_eval_value, used to evaluate a procedure
       before it is applied. Reserved names are simply passed through as
       symbols, which are dealt with in a specific way in
       l_apply_procedure. */
    if (c_symbol_p(proc) && c_reserved_name_p(proc)) {
        *ret = proc;
        return true;
    }
    return l_eval_value(ret, proc, env);
}


static bool l_apply_lambda(
    LispValue* ret,
    LispValue* next_proc, LispValue* next_args, Environment* next_env,
    LispValue proc, LispValue args, Environment env)
{
    assert(c_lambda_p(proc));
    assert(c_list_p(args));

    LispValue params = c_lambda_params(proc);
    LispValue body = c_lambda_body(proc);
    Environment lexical_env = c_lambda_lexical_env(proc);

    if (c_list_length(params) != c_list_length(args)) {
        *ret = c_lsprintf("lambda: wrong number of arguments");
        return false;
    }

    /* Extend the lambda's lexical environment with its parameter
     * bindings. Note the use of 'env' below: arguments are looked
     * up/evaluated in the environment where the lambda is executed,
     * rather than its lexical environment or its internal
     * environment. */
    Environment lambda_env = make_environment(lexical_env);
    LispValue p, a, a_eval;
    while (!c_empty_p(params)) {
        p = c_first(params); a = c_first(args);
        params = c_rest(params); args = c_rest(args);

        /* Evaluate the value to be bound in the current
         * environment. */
        if (!l_eval_value(&a_eval, a, env)) {
            *ret = a_eval;
            return false;
        }

        c_environment_define(lambda_env, p, a_eval);
    }

    /* Evaluate the next procedure */
    if (!l_eval_procedure(next_proc, c_first(body), lambda_env)) {
        *ret = *next_proc;
        return false;
    }

    *next_args = c_rest(body);
    *next_env = lambda_env;

    *ret = NULL;
    return true;
}


static bool l_apply_special_define(LispValue* ret,
                                   LispValue args, Environment env) {
    if (c_list_length(args) != 2) {
        *ret = c_lsprintf("define: wrong number of arguments");
        return false;
    }

    LispValue sym = c_first(args);
    LispValue binding = c_first(c_rest(args));
    LispValue evaluated;
    if (!c_symbol_p(sym)) {
        *ret = c_lsprintf("define: first argument must be a symbol");
        return false;
    }
    if (c_reserved_name_p(sym)) {
        *ret = c_lsprintf("define: can't assign to reserved name '%s'",
                          gcstr_data(c_symbol_value(sym)));
        return false;
    }
    if (!l_eval_value(&evaluated, binding, env)) {
        *ret = evaluated;
        return false;
    }

    c_environment_define(env, sym, evaluated);
    return true;
}


static bool l_apply_special_eval(LispValue* ret,
                                 LispValue* next_proc, LispValue* next_args,
                                 LispValue args, Environment env) {
    LispValue arg, proc;

    /* Check that we have just the one form to eval */
    if (c_list_length(args) != 1) {
        *ret = c_lsprintf("eval: wrong number of arguments");
        return false;
    }

    /* Evaluate the first arg */
    if (!l_eval_value(&arg, c_first(args), env)) {
        *ret = arg;
        return false;
    }

    /* Check that we now have a form, after evaluation */
    if (!c_pair_p(arg)) {
        *ret = c_lsprintf("eval: argument must be a form");
        return false;
    }

    /* Eval the procedure at the head of the form, then set the next
     * procedure and arguments */
    if (!l_eval_procedure(&proc, c_first(arg), env)) {
        *ret = proc;
        return false;
    }
    *ret = NULL; *next_proc = proc; *next_args = c_rest(arg);
    return true;
}


static bool l_apply_procedure(LispValue* ret, Stack stack) {
    bool success, is_eval;
    LispValue next_proc = NULL;
    LispValue next_args = NULL;
    Environment next_env = NULL;

    LispValue current, proc, args, temp;
    Environment env;
    while (stack != NULL) {
        /* Pop from the top of the stack. */
        current = stack->form;
        env = stack->env;
        stack = stack->next;

        /* Destructure the expression */
        if (!c_pair_p(current)) {
            *ret = c_lsprintf("not a form");
            return false;
        }
        proc = c_first(current);
        args = c_rest(current);

        /* First, we evaluate the initial procedure */
        if (!l_eval_procedure(&proc, proc, env)) {
            *ret = proc;
            return false;
        }

        while (true) {
            /* Assess whether this form is an "eval", as we'll need
             * this information several times. */
            is_eval = (c_symbol_p(proc) &&
                       gcstr_eq(c_symbol_value(proc), "eval"));

            /* Check for special forms */
            if (c_symbol_p(proc)) {
                if (gcstr_eq(c_symbol_value(proc), "define")) {
                    if (l_apply_special_define(ret, args, env)) {
                        break;
                    } else {
                        return false;
                    }
                } else if (gcstr_eq(c_symbol_value(proc), "begin")) {
                    /* If we encounter a begin form, we need to push
                       the body of that form onto the stack. */
                    temp = c_list_reverse(args);
                    while (!c_empty_p(temp)) {
                        stack = stack_push(stack, c_first(temp), env);
                        temp = c_rest(temp);
                    }
                    break;
                } else if (!is_eval && c_reserved_name_p(proc)) {
                    *ret = c_lsprintf("'%s' not allowed in this context",
                                      gcstr_data(c_symbol_value(proc)));
                    return false;
                }
            }

            /* Error if we don't have a procedure */
            if (!is_eval && !c_procedure_p(proc)) {
                *ret = c_lsprintf("not a procedure");
                return false;
            }

            /* In the functions below, `next_proc`, `next_args`, and
             * `next_env` are passed as pointer-pointers so that the
             * function to which we dispatch can set the continuation, its
             * arguments and its environment. */
            if (is_eval) {
                success = l_apply_special_eval(ret, &next_proc, &next_args,
                                               args, env);
                next_env = env;
            } else if (c_boolean_p(proc)) {
                success = l_apply_boolean(ret, &next_proc, &next_args,
                                          proc, args, env);
            } else if (c_primitive_p(proc)) {
                success = l_apply_primitive(ret, &next_proc, &next_args,
                                            proc, args, env);
            } else if (c_lambda_p(proc)) {
                success = l_apply_lambda(ret, &next_proc, &next_args, &next_env,
                                         proc, args, env);
            } else {
                *ret = c_lsprintf("unhandled procedure type");
                success = false;
            }

            if (!success) {
                /* If the dispatched function has flagged an error,
                 * return. The l_apply_* functions are responsible for
                 * setting *ret, which in this case should contain an
                 * error message */
                return false;
            }

            if (next_proc == NULL) {
                /* Some procedure has set a null continuation to indicate
                 * that evaluation is complete, so we break. The value of
                 * `*ret` is meaningless in this situation. */
                break;
            }

            /* If the loop is continuing, set the next proc/args/env */
            proc = next_proc; args = next_args; env = next_env;
        }
    }

    return true;
}


bool l_eval_application(LispValue* ret, LispValue exp, Environment env) {
    /* All this does is build a stack from the given expression and
     * environment, and pass through to l_apply_procedure, which is
     * responsible for all the actual work, containing as it does both
     * the stack and TCO loops. */
    return l_apply_procedure(ret, stack_push(NULL, exp, env));
}
