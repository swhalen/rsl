#include "string_utils.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gc/gc.h>


/* Regex helper functions */

RegexResult regex_match(const char* str, const char* pattern) {
    pcre2_code* re;
    int rc;
    int errornumber;
    PCRE2_SIZE erroroffset;
    pcre2_match_data *match_data;
    PCRE2_SIZE* ovector;

    /* Compile the pattern */
    re = pcre2_compile((PCRE2_SPTR)pattern, PCRE2_ZERO_TERMINATED, 0,
                       &errornumber, &erroroffset, NULL);

    /* Perform the match */
    match_data = pcre2_match_data_create_from_pattern(re, NULL);
    rc = pcre2_match(re, (PCRE2_SPTR)str, PCRE2_ZERO_TERMINATED, 0, 0,
                     match_data, NULL);
    ovector = pcre2_get_ovector_pointer(match_data);

    /* Create the return quantity */
    RegexResult result = {(PCRE2_SPTR)str, rc, match_data, ovector};

    /* Free memory */
    pcre2_code_free(re);

    return result;
}

void regex_cleanup(RegexResult r) {
    pcre2_match_data_free(r.match_data);
}

char* regex_result_get_match(RegexResult r, int n) {
    /* Check */
    assert(0 <= n);
    assert(n < r.match_count);

    /* Find the match in question */
    size_t substr_len = r.ovector[2 * n + 1] - r.ovector[2 * n];
    PCRE2_SPTR substr_start = r.subject + r.ovector[2 * n];

    /* Copy to a new string */
    char* result = malloc(substr_len + 1);
    if (result == NULL) {
        printf("Memory error!\n");
        abort();
    }
    memcpy(result, substr_start, substr_len + 1);
    result[substr_len] = '\0';

    return result;
}

bool regex_match_p(const char* str, const char* pattern, bool exact) {
    RegexResult r = regex_match(str, pattern);

    bool result;
    if (r.match_count < 0) {
        /* No match */
        result = false;
    } else if (!exact) {
        /* There's a match, so if an inexact match is OK, then we can
         * return true */
        result = true;
    } else {
        /* Exact match requested: check that the length of the match
         * is equal to the length of the string */
        result = r.ovector[1] - r.ovector[0] == strlen(str);
    }

    regex_cleanup(r);

    return result;
}


/* Garbage-collected strings */

struct _String {
    const char* data;
    size_t len;
};

String gcstr_new(const char* input) {
    /* Make a copy of the string */
    size_t len = strlen(input);
    char* data = GC_MALLOC(len + 1);
    memcpy(data, input, len + 1);
    data[len] = '\0';       /* Belt and braces */

    /* Create the result String */
    String result = GC_MALLOC(sizeof(*result));
    result->data = data;
    result->len = len;

    return result;
}

String gcstr_vsprintf(const char* format, va_list args) {
    va_list args_tmp;
    va_copy(args_tmp, args);
    /* Compute the size of the formatted string */
    int len = vsnprintf(NULL, 0, format, args);

    /* Allocate memory and redo the format, for real this time */
    char* data = GC_MALLOC(len + 1);
    vsnprintf(data, len + 1, format, args_tmp);

    /* Create the result String */
    String result = GC_MALLOC(sizeof(*result));
    result->data = data;
    result->len = len;

    return result;
}

String gcstr_sprintf(const char* format, ...) {
    va_list args;
    String result;

    va_start(args, format);
    result = gcstr_vsprintf(format, args);
    va_end(args);

    return result;
}

const char* gcstr_data(String str) {
    return str->data;
}

size_t gcstr_len(String str) {
    return str->len;
}

String gcstr_slice(String str, size_t start, size_t end) {
    /* Get length and check consistency */
    size_t len = str->len;
    assert((start <= end) && (end < len));
    size_t substr_len = end - start;

    /* Create the substring */
    char* data = GC_MALLOC(substr_len + 1);
    memcpy(data, str->data + start, end);
    data[substr_len] = '\0';

    /* Create the result String */
    String result = GC_MALLOC(sizeof(*result));
    result->data = data;
    result->len = len - 2;

    return result;
}

long gcstr_tol(String str) {
    return atol(str->data);
}

bool gcstr_eq(String str, const char* cmp) {
    return strcmp(cmp, str->data) == 0;
}

bool gcstr_regex_match(String str, const char* pattern, bool exact) {
    return regex_match_p(str->data, pattern, exact);
}
