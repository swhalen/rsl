#ifndef __GCSTR_H__
#define __GCSTR_H__

#include <inttypes.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdarg.h>

/* Regex */

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

typedef struct {
    PCRE2_SPTR subject;
    int match_count;
    pcre2_match_data* match_data;
    PCRE2_SIZE* ovector;
} RegexResult;

RegexResult regex_match(const char*, const char*);

char* regex_result_get_match(RegexResult, int);

void regex_cleanup(RegexResult);

bool regex_match_p(const char*, const char*, bool);

/* Garbage-collected strings */

typedef struct _String *String;

String gcstr_new(const char*);

String gcstr_vsprintf(const char*, va_list);

String gcstr_sprintf(const char*, ...);

const char* gcstr_data(String);

size_t gcstr_len(String);

String gcstr_slice(String, size_t, size_t);

bool gcstr_eq(String, const char*);

long gcstr_tol(String);

bool gcstr_regex_match(String, const char*, bool);

#endif
