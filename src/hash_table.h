#include <stdbool.h>

typedef struct _HashTable *HashTable;

HashTable make_hash_table(void);

void hash_table_insert(HashTable, const char*, void*);

void* hash_table_search(HashTable, const char*);

bool hash_table_delete(HashTable, const char*);
