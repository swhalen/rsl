#ifndef __CPS_TRANSFORM_H__
#define __CPS_TRANSFORM_H__

#include "values.h"

LispValue c_cps_transform(LispValue);

#endif
