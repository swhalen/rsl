#ifndef __READ_H__
#define __READ_H__

#include "values.h"

bool l_read_from_string(LispValue*, const char*);

#endif
