#include "cps_transform.h"

#include <assert.h>             /* TODO: remove, shouldn't be needed */

/* Helpers */

static LispValue HALT() {
    return c_make_symbol(gcstr_new("halt"));
}

static LispValue lambda(LispValue args, LispValue body) {
    return c_make_list(3, c_make_symbol(gcstr_new("lambda")), args, body);
}

/* Predicates */

static bool starts_with(const char* sym, LispValue expr) {
    if (!c_list_p(expr)) {
        return false;
    }
    expr = c_first(expr);
    if (!c_symbol_p(expr)) {
        return false;
    }
    return gcstr_eq(c_symbol_value(expr), sym);
}

static bool lambda_p(LispValue expr) {return starts_with("lambda", expr);}
static bool quoted_p(LispValue expr) {return starts_with("quote", expr);}
static bool begin_p(LispValue expr) {return starts_with("begin", expr);}
static bool define_p(LispValue expr) {return starts_with("define", expr);}
static bool if_p(LispValue expr) {return starts_with("if", expr);}

static bool aexpr_p(LispValue expr) {
    return ((!c_pair_p(expr))
            || lambda_p(expr)
            || quoted_p(expr));
}

/* Forward declarations */

static LispValue M(LispValue);

static LispValue T(LispValue, LispValue);

/* Auxiliary transformer functions */

static LispValue T_define(LispValue expr) {
    LispValue sym = c_first(expr);
    LispValue name = c_first(c_rest(expr));
    LispValue defn = c_first(c_rest(c_rest(expr)));

    return c_make_list(3, sym, name, M(defn));
}

static LispValue T_begin(LispValue expr, LispValue k) {
    /* Transform each form in the body of the begin */
    LispValue temp, new_expr;

    temp = c_rest(expr);
    if (c_empty_p(temp)) {
        /* Empty begin */
        return expr;
    }

    new_expr = c_make_empty();
    temp = c_list_reverse(temp);

    /* The final form takes the continuation k */
    new_expr = c_make_pair(T(c_first(temp), k), new_expr);
    temp = c_rest(temp);

    while (!c_empty_p(temp)) {
        /* Loop through the remaining forms with a 'blank' continuation */
        new_expr = c_make_pair(T(c_first(temp), HALT()), new_expr);
        temp = c_rest(temp);
    }

    return c_make_pair(c_first(expr), new_expr);
}

static LispValue T_if(LispValue expr, LispValue k) {
    LispValue head = c_first(c_rest(expr));
    LispValue tail = c_rest(c_rest(expr));

    LispValue actions = c_make_empty();
    while (!c_empty_p(tail)) {
        actions = c_make_pair(lambda(c_make_empty(), T(c_first(tail), k)),
                              actions);
        tail = c_rest(tail);
    }
    actions = c_list_reverse(actions);

    /* Wrap the list of actions in a lambda and pass as a continuation
     * to the head of the expression */
    LispValue gb = c_make_gensym("b");
    return T(head, lambda(c_make_pair(gb, c_make_empty()),
                          c_make_pair(gb, actions)));
}

static LispValue T_application(LispValue expr, LispValue k) {
    LispValue func, args, gf, ge, ge_list, cont, arg, temp;

    /* Destructure the application*/
    func = c_first(expr);
    args = c_list_reverse(c_rest(expr));

    /* Define gensyms for the intermediate variables */
    ge_list = c_make_empty();
    temp = args;
    while (!c_empty_p(temp)) {
        ge_list = c_make_pair(c_make_gensym("e"), ge_list);
        temp = c_rest(temp);
    }

    /* Make the final continuation */
    gf = c_make_gensym("f");
    cont = c_make_pair(gf, c_make_pair(k, ge_list));

    /* Wrap the final continuation progressively, from the last arg to
       the first (note the line above where the args are reversed). */
    temp = c_list_reverse(ge_list);
    while (!c_empty_p(temp)) {
        /* Pop a gensym and an arg (these should correspond to one
           another), starting from the last and working towards the
           first. */
        ge = c_first(temp); arg = c_first(args);

        /* Build the continuation and transform */
        cont = T(arg, lambda(c_make_pair(ge, c_make_empty()), cont));

        /* Iterate */
        temp = c_rest(temp); args = c_rest(args);
    }

    /* Finally, wrap the entire expression in a lambda, transform and return */
    return T(func, lambda(c_make_pair(gf, c_make_empty()), cont));
}

/* Lambda transformer */

static LispValue M(LispValue expr) {
    LispValue lambda_vars, lambda_body, g;
    if (!lambda_p(expr)) {
        return expr;
    }
    lambda_vars = c_first(c_rest(expr));
    lambda_body = c_first(c_rest(c_rest(expr)));

    g = c_make_gensym("k");

    lambda_vars = c_make_pair(g, lambda_vars);
    lambda_body = T(lambda_body, g);

    return lambda(lambda_vars, lambda_body);
}

/* Main expression transformer */

static LispValue T(LispValue expr, LispValue k) {
    if (define_p(expr)) {
        return T_define(expr);
    } else if (aexpr_p(expr)) {
        return c_make_pair(k, c_make_pair(M(expr), c_make_empty()));
    } else if (begin_p(expr)) {
        return T_begin(expr, k);
    } else if (if_p(expr) && c_list_length(expr) > 1) {
        return T_if(expr, k);
    } else {
        /* Because aexpr_p returns true for any non-pair, this must be
           a pair. The other cases above should deal with any special
           forms; consequently, this is a function application. */
        return T_application(expr, k);
    }
}

/* Interface function */

LispValue c_cps_transform(LispValue expr) {
    return T(expr, HALT());
}
