#include "lib.h"

#include "cps_transform.h"

#include <assert.h>
#include <stdio.h>
#include <gc/gc.h>

/* Fundamental primitives */

static bool repl_halt(LispValue* ret, __attribute__((unused)) LispValue _) {
    *ret = NULL;
    return true;
}

static bool repl_print(LispValue* ret, LispValue message) {
    assert(c_list_p(message));
    while (!c_empty_p(message)) {
        c_print(c_first(message), true, false);
        message = c_rest(message);
        if (!c_empty_p(message)) {
            printf(" ");
        }
    }
    printf("\n");
    *ret = c_make_void();
    return true;
}

static bool cps_transform_prim(LispValue* ret, LispValue args) {
    assert(c_list_p(args));
    if (c_list_length(args) != 1) {
        *ret = c_lsprintf("cps-transform takes exactly one argument");
        return false;
    }

    LispValue transformed = c_cps_transform(c_first(args));
    if (transformed == NULL) {
        /* Just in case, but this should never happened */
        *ret = c_lsprintf("cps-transform returned NULL result");
        return false;
    }

    *ret = c_make_pair(c_make_symbol(gcstr_new("quote")),
                       c_make_pair(transformed, c_make_empty()));
    return true;
}

/* Garbage collection primitives */

static bool l_gc_collect(LispValue* ret, LispValue args) {
    assert(c_list_p(args));
    if (!c_empty_p(args)) {
        *ret = c_lsprintf("too many arguments for 'gc-collect'");
        return false;
    }
    GC_gcollect();
    *ret = c_make_void();
    return true;
}

static bool l_gc_dump(LispValue* ret, LispValue args) {
    assert(c_list_p(args));
    if (!c_empty_p(args)) {
        *ret = c_lsprintf("too many arguments for 'gc-dump'");
        return false;
    }
    GC_dump();
    *ret = c_make_void();
    return true;
}

/* Mathematical built-ins */

typedef int (*BinaryOperator)(int, int);

static bool l_integer_reduction(BinaryOperator op, int initial,
                                LispValue* ret, LispValue args) {
    assert(c_list_p(args));

    int result = initial;
    LispValue cur;
    while (!c_empty_p(args)) {
        cur = c_first(args);

        if (!c_integer_p(cur)) {
            *ret = c_lsprintf("can't do maths with non-integers");
            return false;
        }
        result = op(result, c_integer_value(cur));
        args = c_rest(args);
    }

    *ret = c_make_integer(result);
    return true;
}

static int binary_add(int a, int b) {
    return a + b;
}

static bool l_sum(LispValue* ret, LispValue args) {
    return l_integer_reduction(binary_add, 0, ret, args);
}

static int binary_mul(int a, int b) {
    return a * b;
}

static bool l_product(LispValue* ret, LispValue args) {
    return l_integer_reduction(binary_mul, 1, ret, args);
}

/* Definitions */

static void define_generic_primitive(Environment env, const char* name,
                                     Primitive func, bool direct) {
    c_environment_define(env, c_make_symbol(gcstr_new(name)),
                         c_make_primitive(func, direct));
}


static void define_direct_primitive(Environment env, const char* name,
                                    Primitive func) {
    define_generic_primitive(env, name, func, true);
}


static void define_primitive(Environment env, const char* name,
                             Primitive func) {
    define_generic_primitive(env, name, func, false);
}


Environment make_default_environment() {
    Environment env = make_environment(NULL);

    /* Special 'halt' primitive, that should be the only direct
     * primitive in the language. */
    define_direct_primitive(env, "halt", repl_halt);

    /* Basics */
    define_primitive(env, "print", repl_print);
    define_primitive(env, "cps-transform", cps_transform_prim);

    /* Garbage collection */
    define_primitive(env, "gc-collect", l_gc_collect);
    define_primitive(env, "gc-dump", l_gc_dump);

    /* Arithmetic */
    define_primitive(env, "=", l_equal);
    define_primitive(env, "+", l_sum);
    define_primitive(env, "*", l_product);

    return env;
}
