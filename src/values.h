#ifndef __VALUES_H__
#define __VALUES_H__

#include <stdbool.h>

#include "string_utils.h"

/* Typedefs */

typedef struct _LispValue *LispValue;

typedef struct _Environment *Environment;

typedef bool (*Primitive)(LispValue*, LispValue);

/* Environment manipulation */

Environment make_environment(Environment);

void c_environment_define(Environment, LispValue, LispValue);

bool l_environment_lookup(LispValue*, Environment, LispValue);

/* Helper functions for constructing values */

LispValue c_make_void(void);

LispValue c_make_pair(LispValue, LispValue);

LispValue c_make_empty(void);

LispValue c_make_symbol(String);

LispValue c_make_string(String);

LispValue c_make_boolean(bool);

LispValue c_make_integer(long);

LispValue c_make_primitive(Primitive, bool);

LispValue c_make_lambda(LispValue, LispValue, Environment);

/* Extra helper function for string formatting/literals */

LispValue c_lsprintf(const char*, ...);

/* Lisp type predicates */

bool c_void_p(LispValue);

bool c_pair_p(LispValue);

bool c_empty_p(LispValue);

bool c_list_p(LispValue);

bool c_symbol_p(LispValue);

bool c_string_p(LispValue);

bool c_boolean_p(LispValue);

bool c_integer_p(LispValue);

bool c_direct_primitive_p(LispValue);

bool c_primitive_p(LispValue);

bool c_lambda_p(LispValue);

bool c_procedure_p(LispValue);

bool c_number_p(LispValue);

/* Functions to obtain the contents of a LispValue */

LispValue c_first(LispValue);

LispValue c_rest(LispValue);

String c_symbol_value(LispValue);

String c_string_value(LispValue);

bool c_boolean_value(LispValue);

long c_integer_value(LispValue);

Primitive c_primitive_func(LispValue);

bool c_primitive_direct(LispValue);

LispValue c_lambda_params(LispValue);

LispValue c_lambda_body(LispValue);

Environment c_lambda_lexical_env(LispValue);

/* Utility functions */

long c_list_length(LispValue);

LispValue c_list_reverse(LispValue);

LispValue c_make_list(int, ...);

LispValue c_make_gensym(const char*);

/* Printing */

void c_print(LispValue, bool, bool);

/* Equality */

bool l_equal(LispValue*, LispValue);

#endif
