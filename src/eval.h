#ifndef __EVAL_H__
#define __EVAL_H__

#include "values.h"

bool l_eval_application(LispValue*, LispValue, Environment);

#endif
