#include <assert.h>
#include <stdio.h>
#include <gc/gc.h>

#include "values.h"
#include "hash_table.h"


/* Environments */

struct _Environment {
    Environment parent;
    HashTable data;
};

Environment make_environment(Environment parent) {
    Environment result = GC_MALLOC(sizeof(*result));
    assert(result != NULL);

    result->parent = parent;
    result->data = make_hash_table();

    return result;
}

void c_environment_define(Environment env, LispValue key, LispValue value) {
    assert(c_symbol_p(key));
    const char* key_str = gcstr_data(c_symbol_value(key));
    hash_table_insert(env->data, key_str, value);
}

static LispValue c_environment_lookup_recursive(Environment env,
                                                const char* key_str) {
    if (env == NULL) {return NULL;}
    LispValue result = hash_table_search(env->data, key_str);
    if (result != NULL) {return result;}
    return c_environment_lookup_recursive(env->parent, key_str);
}

bool l_environment_lookup(LispValue* ret, Environment env, LispValue key) {
    assert(c_symbol_p(key));
    const char* key_str = gcstr_data(c_symbol_value(key));
    LispValue result = c_environment_lookup_recursive(env, key_str);
    if (result == NULL) {
        *ret = c_lsprintf("'%s' undefined", key_str);
        return false;
    } else {
        *ret = result;
        return true;
    }
}


/* Lisp expressions and their types */

typedef enum {
    LISP_VOID, LISP_EMPTY, LISP_PAIR, LISP_SYMBOL, LISP_STRING, LISP_BOOLEAN,
    LISP_INTEGER, LISP_PRIMITIVE, LISP_LAMBDA,
} LispValueType;

struct _LispValue {
    LispValueType type;
};

typedef struct {
    LispValue parent;
} *LispVoid;

typedef struct {
    LispValue parent;

    LispValue car;
    LispValue cdr;
} *LispPair;

typedef struct {
    LispValue parent;
} *LispEmpty;

typedef struct {
    LispValue parent;

    String value;
} *LispSymbol;

typedef struct {
    LispValue parent;

    String value;
} *LispString;

typedef struct {
    LispValue parent;

    bool value;
} *LispBoolean;

typedef struct {
    LispValue parent;

    long value;
} *LispInteger;

typedef struct {
    LispValue parent;

    Primitive func;
    bool direct;
} *LispPrimitive;

typedef struct {
    LispValue parent;

    LispValue params;
    LispValue body;
    Environment lexical_env;
} *LispLambda;


/* Constructor functions */

LispValue c_make_void() {
    LispVoid result = GC_MALLOC(sizeof(*result));
    assert(result != NULL);

    ((LispValue)result)->type = LISP_VOID;

    return (LispValue)result;
}

LispValue c_make_pair(LispValue car, LispValue cdr) {
    /* The cdr of a pair should be either a pair itself, or
     * empty. This way, all pairs are proper lists. */
    assert(c_empty_p(cdr) || c_pair_p(cdr));

    LispPair result = GC_MALLOC(sizeof(*result));
    assert(result != NULL);

    ((LispValue)result)->type = LISP_PAIR;

    result->car = car;
    result->cdr = cdr;

    return (LispValue)result;
}

LispValue c_make_empty() {
    LispEmpty result = GC_MALLOC(sizeof(*result));
    assert(result != NULL);

    ((LispValue)result)->type = LISP_EMPTY;

    return (LispValue)result;
}

LispValue c_make_symbol(String value) {
    LispSymbol result = GC_MALLOC(sizeof(*result));
    assert(result != NULL);

    ((LispValue)result)->type = LISP_SYMBOL;

    result->value = value;

    return (LispValue)result;
}

LispValue c_make_string(String value) {
    LispString result = GC_MALLOC(sizeof(*result));
    assert(result != NULL);

    ((LispValue)result)->type = LISP_STRING;

    result->value = value;

    return (LispValue)result;
}

LispValue c_make_boolean(bool value) {
    LispBoolean result = GC_MALLOC(sizeof(*result));
    assert(result != NULL);

    ((LispValue)result)->type = LISP_BOOLEAN;

    result->value = value;

    return (LispValue)result;
}

LispValue c_make_integer(long value) {
    LispInteger result = GC_MALLOC(sizeof(*result));
    assert(result != NULL);

    ((LispValue)result)->type = LISP_INTEGER;

    result->value = value;

    return (LispValue)result;
}

LispValue c_make_primitive(Primitive func, bool direct) {
    LispPrimitive result = GC_MALLOC(sizeof(*result));
    assert(result != NULL);

    ((LispValue)result)->type = LISP_PRIMITIVE;

    result->func = func;
    result->direct = direct;

    return (LispValue)result;
}

LispValue c_make_lambda(LispValue params, LispValue body,
                        Environment lexical_env) {
    assert(c_list_p(params) && c_list_p(body));

    LispLambda result = GC_MALLOC(sizeof(*result));
    assert(result != NULL);

    ((LispValue)result)->type = LISP_LAMBDA;

    result->params = params;
    result->body = body;
    result->lexical_env = lexical_env;

    return (LispValue)result;
}


/* String formatting */

LispValue c_lsprintf(const char* format, ...) {
    va_list args;
    String result;

    va_start(args, format);
    result = gcstr_vsprintf(format, args);
    va_end(args);

    return c_make_string(result);
}


/* Lisp type predicates */

static bool lisp_type_p(LispValue lval, LispValueType ltype) {
    return lval->type == ltype;
}

bool c_void_p(LispValue lval) {
    return lisp_type_p(lval, LISP_VOID);
}

bool c_pair_p(LispValue lval) {
    return lisp_type_p(lval, LISP_PAIR);
}

bool c_empty_p(LispValue lval) {
    return lisp_type_p(lval, LISP_EMPTY);
}

bool c_list_p(LispValue lval) {
    return c_pair_p(lval) || c_empty_p(lval);
}

bool c_symbol_p(LispValue lval) {
    return lisp_type_p(lval, LISP_SYMBOL);
}

bool c_string_p(LispValue lval) {
    return lisp_type_p(lval, LISP_STRING);
}

bool c_boolean_p(LispValue lval) {
    return lisp_type_p(lval, LISP_BOOLEAN);
}

bool c_integer_p(LispValue lval) {
    return lisp_type_p(lval, LISP_INTEGER);
}

bool c_primitive_p(LispValue lval) {
    return lisp_type_p(lval, LISP_PRIMITIVE);
}

bool c_lambda_p(LispValue lval) {
    return lisp_type_p(lval, LISP_LAMBDA);
}

bool c_procedure_p(LispValue lval) {
    return (c_boolean_p(lval)
            || c_primitive_p(lval)
            || c_lambda_p(lval));
}

bool c_number_p(LispValue lval) {
    return (c_integer_p(lval));
}


/* Functions to obtain the contents of a LispValue */

LispValue c_first(LispValue lval) {
    assert(c_pair_p(lval));
    return ((LispPair)lval)->car;
}

LispValue c_rest(LispValue lval) {
    assert(c_pair_p(lval));
    return ((LispPair)lval)->cdr;
}

String c_symbol_value(LispValue lval) {
    assert(c_symbol_p(lval));
    return ((LispSymbol)lval)->value;
}

String c_string_value(LispValue lval) {
    assert(c_string_p(lval));
    return ((LispString)lval)->value;
}

bool c_boolean_value(LispValue lval) {
    assert(c_boolean_p(lval));
    return ((LispBoolean)lval)->value;
}

long c_integer_value(LispValue lval) {
    assert(c_integer_p(lval));
    return ((LispInteger)lval)->value;
}

Primitive c_primitive_func(LispValue lval) {
    assert(c_primitive_p(lval));
    return ((LispPrimitive)lval)->func;
}

bool c_primitive_direct(LispValue lval) {
    assert(c_primitive_p(lval));
    return ((LispPrimitive)lval)->direct;
}

LispValue c_lambda_params(LispValue lval) {
    assert(c_lambda_p(lval));
    return ((LispLambda)lval)->params;
}

LispValue c_lambda_body(LispValue lval) {
    assert(c_lambda_p(lval));
    return ((LispLambda)lval)->body;
}

Environment c_lambda_lexical_env(LispValue lval) {
    assert(c_lambda_p(lval));
    return ((LispLambda)lval)->lexical_env;
}


/* Utility functions */

long c_list_length(LispValue lval) {
    /* TODO: note how the form of this function is exactly the same as
     * c_list_reverse below. It should be easy enough to rewrite these
     * as folds. */
    assert(c_list_p(lval));
    long result = 0;

    if (c_empty_p(lval)) {
        return result;
    }

    LispValue cdr;
    while (true) {
        cdr = c_rest(lval);
        result++;

        if (c_empty_p(cdr)) {
            return result;
        } else {
            lval = cdr;
        }
    }
}

LispValue c_list_reverse(LispValue lval) {
    assert(c_list_p(lval));     /* Only works on proper lists */
    LispValue result = c_make_empty();

    if (c_empty_p(lval)) {
        return result;
    }

    LispValue car;
    LispValue cdr;
    while (true) {
        car = c_first(lval);
        cdr = c_rest(lval);
        result = c_make_pair(car, result);

        if (c_empty_p(cdr)) {
            return result;
        } else {
            lval = cdr;
        }
    }
}

LispValue c_make_list(int count, ...) {
    LispValue result = c_make_empty();
    va_list args;
    va_start(args, count);
    for (int jj = 0; jj < count; jj++) {
        result = c_make_pair(va_arg(args, LispValue), result);
    }
    va_end(args);
    return c_list_reverse(result);
}


static long GENSYM_COUNT = 0;

LispValue c_make_gensym(const char* base) {
    LispValue g = c_make_symbol(gcstr_sprintf("$%s%ld", base, GENSYM_COUNT));
    GENSYM_COUNT++;
    return g;
}


/* Printing */

static void print_list(LispValue lval, bool quote_exp, bool quote_str) {
    if (quote_exp) {printf("'");}
    printf("(");
    while (true) {
        if (!c_pair_p(lval)) {
            printf(". ");
            c_print(lval, false, quote_str);
            break;
        }
        c_print(c_first(lval), false, quote_str);
        lval = c_rest(lval);
        if (c_empty_p(lval)) {
            break;
        }
        printf(" ");
    }
    printf(")");
}

void c_print(LispValue lval, bool quote_exp, bool quote_str) {
    if (c_void_p(lval)) {
        printf("<void>");
        return;
    }

    if (c_pair_p(lval)) {
        print_list(lval, quote_exp, true);
        return;
    }

    if (c_empty_p(lval)) {
        if (quote_exp) {printf("'");}
        printf("()");
        return;
    }

    if (c_symbol_p(lval)) {
        if (quote_exp) {printf("'");}
        printf("%s", gcstr_data(c_symbol_value(lval)));
        return;
    }

    if (c_string_p(lval)) {
        if (quote_str) {printf("\"");}
        printf("%s", gcstr_data(c_string_value(lval)));
        if (quote_str) {printf("\"");}
        return;
    }

    if (c_boolean_p(lval)) {
        printf("#%c", c_boolean_value(lval) ? 't' : 'f');
        return;
    }

    if (c_integer_p(lval)) {
        printf("%ld", c_integer_value(lval));
        return;
    }

    if (c_primitive_p(lval)) {
        printf("<primitive>");
        return;
    }

    if (c_lambda_p(lval)) {
        printf("<lambda>");
        return;
    }

    printf("<?>");
}


/* Equality */

static bool same_type(LispValue x, LispValue y) {
    return x->type == y->type;
}

bool l_equal(LispValue* ret, LispValue args) {
    /* TODO: Make this variadic. */
    assert(c_list_p(args));

    if (c_list_length(args) != 2) {
        *ret = c_lsprintf("too many arguments for '='");
        return false;
    }

    LispValue x = c_first(args);
    LispValue y = c_first(c_rest(args));
    if (!same_type(x, y)) {
        *ret = c_make_boolean(false);
        return true;
    }

    if (!(c_integer_p(x) & c_integer_p(y))) {
        *ret = c_lsprintf("equality is only defined for integers");
        return false;
    }

    bool result = c_integer_value(x) == c_integer_value(y);
    *ret = c_make_boolean(result);
    return true;
}
