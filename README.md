rsl
===

`rsl` stands for either "really small lisp" or "really slow lisp". It does not work yet.

Details and conventions of the C implementation
-----------------------------------------------

Interface functions that involve `LispValue` parameters or return values are, by convention, prefixed with either `c_` or `l_`. The `c_` functions are regular C procedures. The `l_` functions are written in a specific style, taking a continuation as their first parameter. Predicate C functions (i.e. those returning `bool`) are suffixed with `_p`.

Bibliography
------------

- H. Abelson, G. J. Sussman, and J. Sussman. [*Structure and Interpretation of Computer Programs*](https://mitpress.mit.edu/sicp/)
- D. Holden. [*Build Your Own Lisp*](http://buildyourownlisp.com/)
- S. Howard. [*Yoctolisp*](https://github.com/fragglet/yoctolisp)
- J. Martin. [*mal - Make a Lisp*](https://github.com/kanaka/mal)

License
-------

`rsl` is licensed under the GNU General Public License, Version 3. See `LICENSE` for details.
