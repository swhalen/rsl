import os
import sys
import glob
import subprocess


class TestFailure(BaseException):
    def __init__(self, message):
        self.message = message


def parse_single_test(code, output):
    test_input = '\n'.join(code)
    expected_output = '\n'.join(l[2:] for l in output)
    return test_input, expected_output


def parse_test_file(testfn):
    with open(testfn) as hdl:
        lines = [l.rstrip('\n') for l in hdl.readlines()] + ['']

    tests = []
    while lines:
        # Get the code
        newline_idx = lines.index('')
        code = lines[:newline_idx]
        lines = lines[newline_idx + 1:]

        # Get the output
        assert lines[0] == '; Output:'
        newline_idx = lines.index('')
        output = lines[1:newline_idx]
        lines = lines[newline_idx + 1:]

        test_input, exp_output = parse_single_test(code, output)
        tests.append((test_input, exp_output))

    test_fn = os.path.splitext(os.path.basename(testfn))[0]
    test_name, test_type = test_fn.split('-')

    return test_name, test_type, tests


def execute_test(code, expected_output, cps=False, binary='./rsl'):
    cmd = [binary]
    if cps:
        cmd.append('--cps')
    p1 = subprocess.Popen(["echo", code], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(cmd, stdin=p1.stdout, stdout=subprocess.PIPE)
    p1.stdout.close()
    output = p2.communicate()[0].decode().rstrip('\n')

    if output != expected_output:
        raise TestFailure(' expected "%s" got "%s"' % (expected_output, output))


def run_tests(testdir, pat, cps, name=None):
    if name:
        print(name)
        print('=' * len(name))
    testfns = glob.glob(os.path.join(testdir, '%s.rsl' % pat))

    passes, fails = 0, 0
    for testfn in testfns:
        try:
            test_name, test_type, tests = parse_test_file(testfn)
        except Exception as e:
            print('Malformed test file "%s".\nEncountered %s: %s' %
                  (testfn, type(e).__name__, str(e)))
            return

        for n, (test, expected_output) in enumerate(tests):
            try:
                execute_test(test, expected_output, cps=cps)
            except TestFailure as e:
                print(test_name, n, 'FAIL:', e.message)
                fails += 1
            else:
                print('.', end='')
                sys.stdout.flush()
                passes += 1
    print()
    print(passes, 'tests passed.')
    print(fails, 'tests failed.')


def main(testdir):
    # CPS tests
    run_tests(testdir, '*-cps', True, 'CPS')
    # Non-CPS tests
    run_tests(testdir, '*-transform', False, 'Transformed')


if __name__ == '__main__':
    import sys
    main(sys.argv[1])
