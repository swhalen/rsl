CC := gcc
CCFLAGS := -std=gnu11 -Wall -Wextra -Wpedantic -pedantic-errors\
	   -fsanitize=address -fsanitize=undefined
LIBS := -lreadline -lgc -lpcre2-8 -lm

SRCDIR = ./src
HEADERS := $(wildcard $(SRCDIR)/*.h) $(wildcard $(SRCDIR)/*.hpp)
SOURCES := $(wildcard $(SRCDIR)/*.c) $(wildcard $(SRCDIR)/*.cpp)

all : rsl

# Git version file

GIT_DIRTY_WARNING = " (with local changes)"
GIT_VERSION := $(shell git describe --abbrev=10 --dirty=$(GIT_DIRTY_WARNING)\
                 --always --tags)
gitversion.c : .git/HEAD .git/index
	@echo -n "Updating $@... "
	@echo "const char *argp_program_version = \"$(GIT_VERSION)\";" > $@
	@echo "done."

# Main

rsl : gitversion.c $(HEADERS) $(SOURCES)
	@echo -n "Building $@... "
	@$(CC) $(CCFLAGS) -o $@ $(SOURCES) $(LIBS)
	@echo "done."

.PHONY : clean test

clean :
	@rm -f rsl gitversion.c

test : rsl
	@python3 test/test.py test
